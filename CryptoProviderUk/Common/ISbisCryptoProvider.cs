﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CryptoProviderUk.Common
{
    public interface ISbisCryptoProvider
    {
        bool IsInstalled();
        string Name();
        string[] GetContainerNames();
        bool DestroyContainer(string container_name);
    }
}
