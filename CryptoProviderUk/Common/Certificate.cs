﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EUSignCP;

namespace CryptoProviderUk.Common
{
    public class Certificate
    {
        private readonly IEUSignCP.EU_CERT_INFO_EX _cetEx;

        public Certificate(IEUSignCP.EU_CERT_INFO_EX cetEx)
        {
            _cetEx = cetEx;
        }

        public bool Load(string cert_data)
        {
            throw new NotImplementedException();
        }

        public bool LoadFromContainer(string container_name)
        {
            throw new NotImplementedException();
            
        }

        public bool InstallIntoContainer(string container_name)
        {
            throw new NotImplementedException();
        }

        public string Data()
        {
            throw new NotImplementedException();
        }

        public bool GetPrivateKey(string container_name, string password)
        {
            throw new NotImplementedException();
            
        }

        public bool HasPrivateKey()
        {
            throw new NotImplementedException();
        }

        public string Issuer()
        {
            return _cetEx.issuer;
        }
        public string ID()
        {
            throw new NotImplementedException();
        }
        public string SerialNumber()
        {
            return _cetEx.serial;
        }
        public string CommonName()
        {
            return _cetEx.subject;
        }

        public string OrganizationalUnitName()
        {
            throw new NotImplementedException();
        }
        public string StateOrProvinceName()
        {
            throw new NotImplementedException();
        }
        public string LocalityName()
        {
            throw new NotImplementedException();
        }
        public string Title()
        {
            throw new NotImplementedException();
        }
        public string OrganizationName()
        {
            throw new NotImplementedException();

        }
        public string EMail()
        {
            throw new NotImplementedException();

        }
        public string FSSRegNum()
        {
            throw new NotImplementedException();
        }
        public string CountryName()
        {
            throw new NotImplementedException();
        }
        public string UnstructuredName()
        {
            throw new NotImplementedException();
        }
        public string SNILS()
        {
            throw new NotImplementedException();
        }
        public string OGRNIP()
        {
            throw new NotImplementedException();
        }
        public string OGRN()
        {
            throw new NotImplementedException();
        }
        public string INN()
        {
            throw new NotImplementedException();
        }
        public string GetPolicies()
        {
            throw new NotImplementedException();
        }
        public string GetEnhancedKeyUsage()
        {
            throw new NotImplementedException();
        }
        public string IssueDateTime()
        {
            throw new NotImplementedException();

        }
        public string ExpirationDateTime()
        {
            throw new NotImplementedException();
        }
        public string SignQualification()
        {
            throw new NotImplementedException();
        }
        public string ShowWindow()
        {
            throw new NotImplementedException();
        }
    }
}
