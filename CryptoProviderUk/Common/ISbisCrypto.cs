﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CryptoProviderUk.Common
{
    public interface ISbisCrypto
    {
        Object CreateObject(string type);
        string CreateDetachedSign(string source_data, Certificate cert);
        string CreateAttachedSign(string source_data, Certificate cert);
        bool DetachedSignIsValid(string source_data, string detached_sign_data);
        string DecryptData(string encrypted_data, Certificate cert);
        string EncryptData(string encrypted_data, Certificate cert, Certificate[] targetCerts);
        void SaveRootCertificates(string[] certBase64);
        Certificate GetDetachedSignerCertificate(string detached_sign_data);
        bool EnableCryptographyLogging(string file_name);
        string LastErrorText();
    }
}
