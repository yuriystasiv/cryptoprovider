﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConnectionConsulting.Crypto;
using CryptoProviderUk.Common;

namespace CryptoProviderUk
{
    public class SbisCryptoUk: ISbisCrypto
    {
        public object CreateObject(string type)
        {
            if (type == "Provider")
            {
                return new SbisCryptoProviderUk();
            }
            throw new NotImplementedException();
        }

        private void CheckPrivateKeyIsReaded(Certificate cert)
        {
            //ToDo: Maybe just read private key... 
            if (!CryptoLibraryHelper.IsPrivateKeyReaded())
                throw new Exception("Private key is not readed");

            var privateKeyCertBinary = CryptoLibraryHelper.GetOwnCertificate();
            var privateKeyCert = CryptoLibraryHelper.ParseCertificate(privateKeyCertBinary);
            if (cert.Issuer() != privateKeyCert.issuer || cert.SerialNumber() != privateKeyCert.serial)
                throw new Exception("Another private key is readed");
        }

        public string CreateDetachedSign(string source_data, Certificate cert)
        {
            CheckPrivateKeyIsReaded(cert);

            var sign = CryptoLibraryHelper.Sign(Convert.FromBase64String(source_data));
            return Convert.ToBase64String(sign);
        }

       

        public string CreateAttachedSign(string source_data, Certificate cert)
        {
            CheckPrivateKeyIsReaded(cert);

            var sign = CryptoLibraryHelper.SignExternal(Convert.FromBase64String(source_data));
            return Convert.ToBase64String(sign);
        }

        public bool DetachedSignIsValid(string source_data, string detached_sign_data)
        {
            if (!CryptoLibraryHelper.IsSign(Convert.FromBase64String(detached_sign_data)))
                return false;
            try
            {
                return CryptoLibraryHelper.CheckSign(Convert.FromBase64String(source_data),
                    Convert.FromBase64String(detached_sign_data));
            }
            catch (LibraryException ex)
            {
                _lastErrorText = ex.Message;
            }
            return false;
        }

        public string DecryptData(string encrypted_data, Certificate cert)
        {
            CheckPrivateKeyIsReaded(cert);
            try
            {
                var developed = CryptoLibraryHelper.Develop(Convert.FromBase64String(encrypted_data));
                return Convert.ToBase64String(developed);
            }
            catch (LibraryException ex)
            {
                _lastErrorText = ex.Message;
            }
            return null;
        }

        public string EncryptData(string encrypted_data, Certificate cert, Certificate[] targetCerts)
        {
            CheckPrivateKeyIsReaded(cert);
            try
            {
                var enveloped = CryptoLibraryHelper.Envelop(Convert.FromBase64String(encrypted_data),
                    targetCerts.Select(item => item.Issuer()).ToArray(),
                    targetCerts.Select(item => item.SerialNumber()).ToArray());
                return Convert.ToBase64String(enveloped);
            }
            catch (LibraryException ex)
            {
                _lastErrorText = ex.Message;
            }
            return null;
        }

        public void SaveRootCertificates(string[] certsBase64)
        {
            foreach (var certBase64 in certsBase64)
            {
                CryptoLibraryHelper.SaveCertificate(Convert.FromBase64String(certBase64));
            }
        }

        public Certificate GetDetachedSignerCertificate(string detached_sign_data)
        {
            try
            {
                var cert = CryptoLibraryHelper.GetSignerInfo(Convert.FromBase64String(detached_sign_data));
                Certificate certificate = new Certificate(cert);//ToDo: Fill certificate

                return certificate;   
            }
            catch (LibraryException ex)
            {
                _lastErrorText = ex.Message;
            }
            return null;
        }

        public bool EnableCryptographyLogging(string file_name)
        {
            //ToDo:
            throw new NotImplementedException();
        }

        private string _lastErrorText;
        public string LastErrorText()
        {
            return _lastErrorText;
        }
    }
}
